import time
from math import sqrt, floor, ceil

def calculate_time(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"Time taken to execute {func.__name__}: {end_time - start_time} seconds")
        return result
    return wrapper

@calculate_time
"""Принимает список целых чисел и возращает новый список,в котором 
целое число заменено квадратным корнем округленным до ближайшего числа
  """
def round_sqrt(numbers, direction='down'):
    new_numbers = []
    for num in numbers:
        if direction == 'down':
            new_numbers.append(floor(sqrt(num)))
        elif direction == 'up':
            new_numbers.append(ceil(sqrt(num)))
    return new_numbers

numbers = [2, 4, 9, 16, 25]
direction = 'up'  # or 'down'
result = round_sqrt(numbers, direction=direction)
print(result)
